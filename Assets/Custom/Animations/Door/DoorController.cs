﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour {

	public Animator doorAnimator;
	public GameObject doorCollider;
	private bool open = false;
	private bool trigger = false;
	void Update () {
		if (Input.GetKeyDown("e") && !open && trigger && doorAnimator.GetCurrentAnimatorStateInfo(0).IsName("Closed")) {
			doorAnimator.Play("opening");
			open = true;
		}
		if (Input.GetKeyDown("e") && open && trigger && doorAnimator.GetCurrentAnimatorStateInfo(0).IsName("open")) {
			doorAnimator.Play("closing");
			open = false;
		}
	}
	void OnTriggerEnter(Collider col) {
		if (col.gameObject.name == doorCollider.name) {
			trigger = true;
		}
	}
	void OnTriggerExit(Collider col) {
		if (col.gameObject.name == doorCollider.name) {
			trigger = false;
		}
	}
	
}
