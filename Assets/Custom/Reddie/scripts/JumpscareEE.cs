﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpscareEE : MonoBehaviour {

	public Animator ReddieAnimator;
	void OnTriggerEnter(Collider col) {
		if (col.tag == "EEJump") {
			ReddieAnimator.Play("EAJumpscare");
		}
	}
}
