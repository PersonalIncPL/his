﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReddieHide : MonoBehaviour {
	
	public Animator anim;
	private bool shown = true;
	
	void OnTriggerEnter (Collider col)
	{
		if(col.tag == "Reddie_Trigger" && shown)
		{
			shown = false;
			anim.Play("Reddie_Hide", -1, 0f);
		}
	}
}
