﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShot : MonoBehaviour {

	// Use this for initialization
	void Update() {
		if (Input.GetKeyDown("k")) {
			String filePath = Application.dataPath.Replace("/", "\\");
			print ("Saving screenshot as " + "screenshot_" + DateTime.Now.ToString("dd MM yyyy HH mm ss") + ".png");
			ScreenCapture.CaptureScreenshot(filePath + "\\screenshot_" + DateTime.Now.ToString("dd MM yyyy HH mm ss") + ".png");
		}
	}
}
