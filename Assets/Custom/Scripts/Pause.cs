﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class Pause : MonoBehaviour {

	private bool isPaused = false;
	public Canvas unpausedCanvas;
	public Canvas pausedCanvas;
	public UnityStandardAssets.Characters.FirstPerson.FirstPersonController controller;
	public Button mainMenuButton;
	public Button restartButton;
	public Toggle postProcessingToggle;
	public PostProcessingProfile postProcessingProfile;
	private String filePath;

	void Start () {
		filePath = Application.dataPath.Replace("/", "\\") + "\\settings.txt";
		try {
			if (File.ReadAllText(filePath) == "on") {
				TogglePostProcessing(true);
				postProcessingToggle.isOn = true;
			} else if (File.ReadAllText(filePath) == "off") {
				TogglePostProcessing(false);
				postProcessingToggle.isOn = false;
			}
		} catch (FileNotFoundException e) {
			print (e);
			TogglePostProcessing(true);
			postProcessingToggle.isOn = true;
		}
		postProcessingToggle.onValueChanged.AddListener(PostProcessing);
		Button btn1 = mainMenuButton.GetComponent<Button>();
		Button btn2 = restartButton.GetComponent<Button>();
		btn1.onClick.AddListener(MainMenu);
		btn2.onClick.AddListener(Restart);
	}
	void PostProcessing(bool state)
    {
		if (state) {
			File.WriteAllText(filePath, "on");
		} else if (!state) {
			File.WriteAllText(filePath, "off");
		}
        TogglePostProcessing(state);
    }
	void MainMenu() {
		unpause();
		SceneManager.LoadScene("MainMenu");
	}
	void Restart() {
		unpause();
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
	void Update () {
		if (Input.GetKeyDown("p") && !isPaused) {
			pause();
		} else if (Input.GetKeyDown("p") && isPaused) {
			unpause();
		}
		
	}

	void pause() {
		unpausedCanvas.enabled = false;
		pausedCanvas.enabled = true;
		controller.enabled = false;
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
		Time.timeScale = 0.0F;
		isPaused = true;
	}
	void unpause() {
		unpausedCanvas.enabled = true;
		pausedCanvas.enabled = false;
		controller.enabled = true;
		Time.timeScale = 1.0F;
		isPaused = false;
	}
	void TogglePostProcessing(bool state) {
		postProcessingProfile.antialiasing.enabled = state;
		postProcessingProfile.ambientOcclusion.enabled = state;
		postProcessingProfile.motionBlur.enabled = state;
		postProcessingProfile.dithering.enabled = state;
		postProcessingProfile.eyeAdaptation.enabled = state;
		postProcessingProfile.colorGrading.enabled = state;
		postProcessingProfile.fog.enabled = state;
		postProcessingProfile.vignette.enabled = state;
	}
}
