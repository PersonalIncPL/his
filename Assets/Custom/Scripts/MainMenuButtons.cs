﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuButtons : MonoBehaviour {

	public Button startGame, endGame;
	public Toggle postProcessingToggle;
	public PostProcessingProfile postProcessingProfile;
	private String filePath;
	void Start ()
	{
		filePath = Application.dataPath.Replace("/", "\\") + "\\settings.txt";
		try {
			if (File.ReadAllText(filePath) == "on") {
				TogglePostProcessing(true);
				postProcessingToggle.isOn = true;
			} else if (File.ReadAllText(filePath) == "off") {
				TogglePostProcessing(false);
				postProcessingToggle.isOn = false;
			}
		} catch (FileNotFoundException e) {
			print (e);
			TogglePostProcessing(true);
			postProcessingToggle.isOn = true;
		}
		Button btn = startGame.GetComponent<Button>();
		Button btn2 = endGame.GetComponent<Button>();
		postProcessingToggle.onValueChanged.AddListener(PostProcessing);
		btn.onClick.AddListener(StartGame);
		btn2.onClick.AddListener(StopGame);
	}

    void PostProcessing(bool state)
    {
		if (state) {
			File.WriteAllText(filePath, "on");
		} else if (!state) {
			File.WriteAllText(filePath, "off");
		}
        TogglePostProcessing(state);
    }

    void StartGame () {
		SceneManager.LoadScene("Map");
	}

	void StopGame () {
		Application.Quit();
	}
	void TogglePostProcessing(bool state) {
		postProcessingProfile.antialiasing.enabled = state;
		postProcessingProfile.ambientOcclusion.enabled = state;
		postProcessingProfile.motionBlur.enabled = state;
		postProcessingProfile.dithering.enabled = state;
		postProcessingProfile.eyeAdaptation.enabled = state;
		postProcessingProfile.colorGrading.enabled = state;
		postProcessingProfile.fog.enabled = state;
		postProcessingProfile.vignette.enabled = state;
	}
}
