﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour {
	public Animator CameraAnimator;
	void OnTriggerEnter(Collider other) {
		if (other.tag == "EEJump") {
			CameraAnimator.Play("Jumpscare");
		}
	}
}
